# Начало работы

## Устанавливаем необходимый софт: 

* [Git](http://git-scm.com/download/) 
* [nodejs & npm](http://nodejs.org/download/)


## Структура репозитория:

```
frontend-test/
├── app/
│   ├── css/
│   │   └── vendor/
│   ├── img/
│   ├── lib/
│   │   └── vendor/
│   ├── styl/
│   ├── index.html
│   ├── Gruntfile.js
│   ├── package.json
│   └── README.md
├── design/
│   ├── test-page.png
│   └── test-page.psd
└── README.md
```


## Клонируем репозиторий:

```
git clone git@bitbucket.org:3davinci/frontend-test.git tdv_test
```


## Переходим в папку `app`:

```
cd tdv_test/app/
```


## Устанавливаем зависимости:

```
npm i
```


## Для удобства устанавливаем глобально grunt-cli:

```
npm i -g grunt-cli
```


## Запускаем отслеживание изменений файлов:

```
grunt
```


## Работаем!

Ваша верстка будет доступна по адресу [http://localhost:9090/](http://localhost:9090/)