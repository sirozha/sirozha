$(document).ready(function(){
	
  $('.city-select__btn').on('click', scrollList);

  $('.city-select__item').on('click', function() {
    $('.city-select__item').removeClass('city-select__item_active');
    $(this).addClass('city-select__item_active');
  });

  function scrollList() {
    
    var container = +$('.city-select__container').outerHeight();
    var list = +$('.city-select__list').outerHeight();

    var upOrDown = 1;

    if ($(this).hasClass('city-select__btn_up'))
      upOrDown = -1;

    var step = upOrDown*$('.city-select__list').children('li').first().outerHeight();

    $('.city-select__list').finish();

    step += parseInt($('.city-select__list').css('marginTop'));

    if (step > 0) {
      step = 0;
    }

    if (container-list >= step) {
    	step = container-list;
    }
    
    if (step == 0) {
      $('.city-select__btn_down').addClass('city-select__btn_disable');
    }

    if (step == container-list) {
      $('.city-select__btn_up').addClass('city-select__btn_disable');
    }

    if (step < 0) {
      $('.city-select__btn_down').removeClass('city-select__btn_disable');
    }

    if (step > container-list) {
      $('.city-select__btn_up').removeClass('city-select__btn_disable');
    }

    $('.city-select__list').animate({marginTop: step}, 300);

  }  
});