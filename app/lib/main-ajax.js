$(document).ready(function(){
  
  $('.city-select__list li').on('click', ajaxQuery);

  function ajaxQuery(){
    $.ajax({
      beforeSend: function() {
        $('.col-xs-10').html('<div class="loading"><img src="img/loading.gif"/><div>');
      },
      complete: function() {
      	setTimeout( function() {
      	  var url = 'ajax.html';
          $('.col-xs-10').load(url);
        }, 2000);
      }
    });
  }
});

